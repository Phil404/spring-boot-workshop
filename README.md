# Spring Boot Workshop
This Workshop is about Spring Boot and getting more in touch with it.  
You will create your own Spring Boot Application from scratch and forming it into a cool Webservice.  
The focus will be on fun and collaboration!  
If you have a complete different idea what you will do with spring, feel free and hit me up :-)
> Notice: this workshop is for you, it's in your hand how much you will get out of it!

# Quick start:
You can simply create a new Spring Boot project via: https://start.spring.io/  
There you can get a quick start config by clicking your preferenced spring packages.  
Here are some recommend packages you might need:
- Spring Web (For build a web service with frontend or REST)
- One of the Template Engines (If you don't use Vue or something like that)
- Spring Security (For secure your application / easy to configure)
- Spring Data JPA (Most common sql package)

> Notice: when you create your project configure the meta data.
Define your java version (13 recommended) and package name

# Minimum Requirements:
- Authentification of users
  - Open / Auth Pages
- Database (e.G. for auth)
- Content Management via Frontend

# Some Ideas for building:
- Webshop
    - With product lists, product page and shopping cart.  
    Maybe a frontend to create a new product.
- Blog
    - Create your own blog, where you can write articles (in a restricted area) and publish them.
- Social Network
- Your own Website
## More technical
- Hosting in Cloud
- Using CI/CD via GitLab
- Building a Vue.js Frontend with a REST APi via Spring Boot

# Office Hours:
Place: Collabor8  
Monday, 28.10 von 09:00 – 12:00 (Kick-Off + Planning)  
Friday, 1.11 von 09:00 – 12:00 (Optional)  
Friday, 8.11 von 09:00 – 12:00  
Friday, 15.11 von 09:00 – 12:00  
Friday, 22.11 von 09:00 – 12:00 (Presentation)

## Expectation of the Kick-Off + Planning:
Everyone has ...
- a Group (at least two members)
- an Idea
- some Issues for their project
- a finished and working setup of spring

#### For questions you can reach me via: philipp.westphalen@adsoul.com